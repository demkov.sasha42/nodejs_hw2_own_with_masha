const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

app.use(
  express.urlencoded({
    extended: true,
  }),
);

const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');
const { authRouter } = require('./authRouter');


app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

async function start() {
  await mongoose.connect('mongodb+srv://oleksandr_ch:salvator86@cluster0.pmlbz8i.mongodb.net/?retryWrites=true&w=majority');
  app.listen(8080);
}

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
