const express = require('express');

const router = express.Router();
const {
  addUserNotes, getNote, deleteNote, getUserNotes, updateMyNoteById,
  markMyNoteCompletedById,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, addUserNotes);

router.get('/', authMiddleware, getUserNotes);

router.get('/:id', getNote);

router.put('/:id', authMiddleware, updateMyNoteById);

router.patch('/:id', authMiddleware, markMyNoteCompletedById);

router.delete('/:id', deleteNote);

module.exports = {
  notesRouter: router,
};
