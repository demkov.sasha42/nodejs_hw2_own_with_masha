const mongoose = require('mongoose');

const notesSchema = mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  createdDate: {
    type: Object
  }
});

const Notes = mongoose.model('notes', notesSchema);

module.exports = {
  Notes,
};
