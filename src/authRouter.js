const express = require('express');
const router = express.Router();
const { createProfile, login } = require('./authService.js');
// const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/register', createProfile);

router.post('/login', login);

module.exports = {
  authRouter: router,
};
